import React, { Component, Fragment } from 'react';
import { UserContext } from './UserContext';

export default class User extends Component {
  render() {
    return (
      <UserContext.Consumer>
        {(context) => (
          <Fragment>
            <div>{context.user.name}</div>
            <button onClick={context.logout}>Logout</button>
          </Fragment>
        )}
      </UserContext.Consumer>
    )
  }
}
