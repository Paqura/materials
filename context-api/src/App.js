import React, { Component } from 'react';
import {UserContext} from './UserContext';
import User from './User';

class UserProvider extends Component {
  state = {
    id: '1',
    name: 'Bill'
  };

  logout = () => {
    this.setState({
      id: null,
      name: 'unknown'
    })
  }

  render() {
    return(
      <UserContext.Provider
        value={{
          user: this.state,
          logout: this.logout
        }}
      >
        {this.props.children}
      </UserContext.Provider>
    )
  }
}

class App extends Component {
  render() {
    return (
      <UserProvider>
        <div className="App">
          <User />
        </div>
      </UserProvider>
    );
  }
};

export default App;
